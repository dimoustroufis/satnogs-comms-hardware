# SatNOGS COMMS hardware

A CubeSat format ([LibreCube Board](https://librecube.gitlab.io/standards/board_specification/)) COMMS board.
It supports UHF and S-Band concurrent half duplex (in each band) communication.
More details could be found [here](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc).

More details about versioning (Go/NoGo is referred to production of this version from other teams, please review the experiments):

| Version | Bug Report | Go/NoGo | Experiments | Software |
| ------ | ------ | ------ | ------ | ------ |
| [Gerber - 0.1.1](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-hardware/-/releases/0.1.1-production) | [Report](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-hardware/-/wikis/SatNOGS-COMMS-v0.1.1,-assembly-changes) | NoGo | [Experiment - 0.1.1](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-hardware/-/wikis/home#satnogs-comms-v011) | N/A |
| [Gerber - 0.2.1](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-hardware/-/releases/0.2.1-assembly-1), [iBom - 0.2.1](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-hardware/-/releases/0.2.1-assembly-1) | [Report](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-hardware/-/wikis/SatNOGS-COMMS-v0.2.1-bug-report) | NoGo |[Experiment - 0.2.1](/uploads/177fbdbe55f0e27b0d8c33624cb746da/test-report.pdf), [Environmental Experiment - 0.2.1](/uploads/f2a2fe56697624136354e35655c860d5/environmental-test-report.pdf), [Power Consumption Table - 0.2.1](https://cloud.libre.space/s/3ajfRzbSNGH4o7m) | N/A |
| [Gerber - 0.3](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-hardware/-/releases/0.3-production-1), [iBom - 0.3](https://gitlab.com/librespa99cefoundation/satnogs-comms/satnogs-comms-hardware/-/releases/0.3-assembly-1) | [Report](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-hardware/-/wikis/SatNOGS-COMMS-v0.3.0-bug-report) | NoGo | [Experiment - 0.3](/uploads/762667179a81d40d56f5acf71dfddb9a/test-report.pdf) | [FM software for the Curium-1 satellite on the Ariane 6 maiden flight](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-software-mcu/-/releases/curium-1) |

We follow [Semantic Versioning 2.0.0](https://semver.org/).

## Join

[![irc](https://img.shields.io/badge/Matrix-%23satnogs_comms:matrix.org-blue.svg)](https://matrix.to/#/#satnogs-comms:matrix.org)


## Contribute
This is a KiCAD project thus merging can be tricky.
Coordinate with project engineers before starting any changes and take look at issues.
Also follow [contribution guide](CONTRIBUTING.md).


### EDA
KiCAD 8

Requires [lsf-kicad-lib](https://gitlab.com/librespacefoundation/lsf-kicad-lib) to be installed.
Set path LSF_KICAD_LIB to point to lsf-kicad-lib folder for 3D models to appear correctly

#### Schematic
Changes in manufacturer part numbers field, footprints and designators can be merged most of the times.
Any part change, change in placement, wire placement etc must be communicated first via issues or [matrix channel](https://matrix.to/#/#satnogs-comms:matrix.org).

One exception is Hierarchical sheets provided that:

* A single person is working on a Hierarchical sheet
* Annotation is using sheet number
* Global nets are respected
* Changes in Hierarchical labels are coordinated

#### PCB
At this point any merging on PCB file must be communicated first via issues or [matrix channel](https://matrix.to/#/#satnogs-comms:matrix.org).

Some useful tips:
* for tracks use grid 0.0254mm
* for fill zones or keep out areas use 0.254mm

### Simulation
[Qucs-S](https://ra3xdh.github.io/), Qucsator 0.0.20 and ngspice-40

### CAD
[FreeCAD Link Daily](https://github.com/realthunder/FreeCAD/releases).


## License
Licensed under the [CERN OHLv1.2](LICENSE)
[![Libre Space Foundation](https://img.shields.io/badge/%C2%A9%202014--2024-Libre%20Space%20Foundation-6672D8.svg)](https://librespacefoundation.org/)
