<Qucs Schematic 2.1.0>
<Properties>
  <View=-148,-77,857,1082,0.774337,0,81>
  <Grid=10,10,1>
  <DataSet=rf-mixer-qucs.dat>
  <DataDisplay=rf-mixer-qucs.dpl>
  <OpenDisplay=0>
  <Script=rf-mixer-qucs.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
  <Ellipse 120 -40 40 40 #0000ff 2 1 #c0c0c0 1 0>
  <Line 150 -30 -20 20 #0000ff 2 1>
  <Line 140 10 0 -10 #000080 2 1>
  <.PortSym 140 10 3 90>
  <Line 110 -20 10 0 #000080 2 1>
  <.PortSym 110 -20 2 0>
  <.ID 100 -86 SUB>
  <Line 160 -20 10 0 #000080 2 1>
  <.PortSym 170 -20 1 180>
  <Line 130 -30 20 20 #0000ff 2 1>
</Symbol>
<Components>
  <GND * 1 230 200 0 0 0 0>
  <GND * 1 400 130 0 0 0 0>
  <Port Out1 1 220 120 -23 12 0 0 "1" 1 "analog" 0 "v" 0 "" 0>
  <EDD D3 1 490 300 -35 -141 0 0 "explicit" 0 "3" 0 "V2*V3" 1 "0" 0 "0" 0 "0" 0 "0" 0 "0" 0>
  <GND * 1 550 410 0 0 0 0>
  <Port In1 1 310 300 -23 12 0 0 "2" 1 "analog" 0 "v" 0 "" 0>
  <Port In2 1 310 360 -23 12 0 0 "3" 1 "analog" 0 "v" 0 "" 0>
  <CCVS SRC1 1 320 150 -26 34 1 2 "1 Ohm" 1 "0" 0>
</Components>
<Wires>
  <400 120 400 130 "" 0 0 0 "">
  <520 300 550 300 "" 0 0 0 "">
  <520 240 550 240 "" 0 0 0 "">
  <550 240 550 300 "" 0 0 0 "">
  <550 300 550 360 "" 0 0 0 "">
  <550 360 550 410 "" 0 0 0 "">
  <520 360 550 360 "" 0 0 0 "">
  <400 180 400 240 "" 0 0 0 "">
  <400 240 460 240 "" 0 0 0 "">
  <310 300 460 300 "" 0 0 0 "">
  <310 360 460 360 "" 0 0 0 "">
  <230 180 230 200 "" 0 0 0 "">
  <350 120 400 120 "" 0 0 0 "">
  <350 180 400 180 "" 0 0 0 "">
  <230 180 290 180 "" 0 0 0 "">
  <220 120 290 120 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
