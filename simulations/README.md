# Simulations
In order to use schematics and to design the RFFE in both radios, open projects
uhf_prj and s-band_prj with [Qucs-s](https://ra3xdh.github.io/) with Qucsator 0.0.20 and ngspice-40.

## UHF project
The schematics need some s-parameter files that you can download from the IC fabricator site.
Place everything under the project directory.

### Schematics
* pa_matching_425.sch: [development board tuned at 425MHz](https://store.qorvo.com/products/detail/tqp7m9106pcb900-qorvo/457244/)
* notch-s-band.sch: input filter for RX to cut off S-Band transmissions
* resistive-taps-agc.sch: Resistive tap to measure the received power for AGC
* pa_matching_940.sch: [development board tuned at 940MHz](https://store.qorvo.com/products/detail/tqp7m9106pcb900-qorvo/457244/)
* pa_matching_425_pcb.sch: SatNOGS COMMS board tuned at 425MHz

### S-Parameters
* [TQP7M9106.s2p](https://www.qorvo.com/products/p/TQP7M9106#documents)
* [LFCG-1400+_Plus25degC.s2p](https://www.minicircuits.com/WebStore/dashboard.html?model=LFCG-1400%2B)
* [LFCG-490+_Plus25degC.s2p](https://www.minicircuits.com/WebStore/dashboard.html?model=LFCG-490%2B)
* [F2972_RF2_ON_EN_1_VCTL_1_50Ohms.s3p](https://www.renesas.com/us/en/products/rf-products/rf-switches/f2972-high-linearity-broadband-sp2t-rf-switch#design_development)
* [F2972_RF1_ON_EN_1_VCTL_0_50Ohms.s3p](https://www.renesas.com/us/en/products/rf-products/rf-switches/f2972-high-linearity-broadband-sp2t-rf-switch#design_development)

## S-Band project
The schematics need some s-parameter files that you can download from the IC fabricator site.
Place everything under the project directory.

### Schematics
* notch-uhf.sch: input filter for RX to cut off UHF transmissions
* pa.sch: Compare small signal gain of different PAs
* resistive-taps-agc.sch: resistive tap to measure the received power for AGC
* rf-mixer-artifacts.sch: calculate the RF Mixer atrifacts by using ngspice-40 with Qucs-s
* rf-mixer-qucs.sch: RF-Mixer subcircuit or Qucsator
* selectable-filters.sch: an experiment to implement a selectable filter mechanism with one RF switch, Reflective Open or Reflective Short
* rx-image-rejection-filter.sch: input filter for RX to cut off RF mixer artifacts
* tx-chain.sch: S-Parameter simulation of TX chain
* tx-image-rejection-filter-pcb.sch: LPF for TX that cut off RF mixer artifacts in SatNOGS COMMS v0.1.1, includes a worst case analysis and optimization to find the proper passive components

### S-Parameters
* [HFCN-1000+ _AP130157_022513_UNIT-1.s2p](https://www.minicircuits.com/WebStore/dashboard.html?model=HFCN-1000%2B)
* [SKY66294_11_sparam_de_embededed.S2P](https://www.skyworksinc.com/Products/Amplifiers/SKY66294-11)
* [SKY66292_11_sparam_de_embedded.S2P](https://www.skyworksinc.com/Products/Amplifiers/SKY66292-11)
* [HMC544A_RF2sel_A_5V_B_0V_deembed.s3p](https://www.analog.com/en/products/hmc544a.html)
* [HMC544A_RF1sel_A_0V_B_5V_deembed.s3p](https://www.analog.com/en/products/hmc544a.html)
* [BFCN-2491+_CE1210_061720_Unit-1.s2p](https://www.minicircuits.com/WebStore/dashboard.html?model=BFCN-2491%2B)
* [F2972_RF2_ON_EN_1_VCTL_1_50Ohms.s3p](https://www.renesas.com/us/en/products/rf-products/rf-switches/f2972-high-linearity-broadband-sp2t-rf-switch#design_development)
* [F2972_RF1_ON_EN_1_VCTL_0_50Ohms.s3p](https://www.renesas.com/us/en/products/rf-products/rf-switches/f2972-high-linearity-broadband-sp2t-rf-switch#design_development)
* [DEA142450BT-3028A1.s2p](https://product.tdk.com/en/search/rf/rf/filter/info?part_no=DEA142450BT-3024A1)
* [1720BL15B0200.s3p](https://www.johansontechnology.com/baluns?option=com_products&id=1720BL15B0200001E)
* [HFCN-2100+.S2P](https://www.minicircuits.com/WebStore/dashboard.html?model=HFCN-2100%2B)
* [QPA0363A_De-embedded_SPARAM.s2p](https://www.qorvo.com/products/p/QPA0363A#documents)
* [2450BL15B050.s3p](https://www.johansontechnology.com/?option=com_products&id=2450BL15B050)
